section .text


; Принимает код возврата и завершает текущий процесс
exit:
  mov rax, 60
  syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
  xor rax, rax
  .loop:
  cmp byte [rdi + rax], 0
  je .end
  inc rax
  jmp .loop
  .end:
  ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
   push rdi
   call string_length
   mov rdx, rax
   mov rax, 1
   mov rdi, 1
   pop rsi
   syscall
   ret

; Принимает код символа и выводит его в stdout
print_char:
  push rdi
  mov rdi, rsp
  call print_string
  add rsp, 8
  ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
  mov rdi, 0xA
  jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
  mov rax, rdi

  xor r10, r10 ;счетчик кол-ва цифр в числе
  ; имеет смысл поэкономить память, т.к размер стека может получиться внушительным
  mov r9, 10			;Основания системы счисления
  .new_word:
  xor r8, r8 ;заведем отдельный счетчик,чтобы не проверять кратность и не перетаскивать значения из rax и обратно
  push 0
  .loop:
  xor rdx, rdx
  div r9

  shl dword [rsp], 8 ;уплотняем

  add byte [rsp], dl ;берем последнюю цифру числа
  inc r10
  inc r8

  test rax, rax	;Проверка на наличие еще цифр в числе
  je .print

  cmp r8, 4 ;проверяем, достигнут ли лимит слова
  jne .loop
  jmp .new_word

  .next_word:
  mov r8, 4
  add rsp, 8
  .print:
  xor rdi, rdi
  cmp r10, 0
  je .end
  cmp r8, 0
  je .next_word

  mov dil, byte[rsp]
  add rdi, '0'
  push r9
  push r10
  call print_char
  pop r10
  pop r9
  shr dword [rsp], 8

  dec r8
  dec r10

  jmp .print
  .end:
  add rsp, 8
  ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
  test rdi, rdi
  jns print_uint
  push rdi
  mov rdi, '-'
  call print_char
  pop rdi
  neg rdi
  jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
  xor rax, rax
  xor r8, r8
  .loop:
  mov al, [rdi+r8]
  cmp al, [rsi+r8]
  jne .unequal
  cmp al, 0
  je .equal
  inc r8
  jmp .loop
  .equal:
  mov rax, 1
  ret
  .unequal:
  mov rax, 0
  ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
  push rdi
  push rsi
  push rcx
  push 0
  xor rax, rax
  xor rdi, rdi
  mov rsi, rsp
  mov rdx, 1
  syscall
  pop rax
  pop rcx
  pop rsi
  pop rdi
  cmp rax, 0xA          ; код перевода строки
  jz .return_0
  ret
  .return_0:
    mov rax, 0
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
  xor r8, r8
  cmp rsi, r8
  jng .error
  .skip:
  call read_char
  cmp rsi, r8
  jng .error
  cmp rax, 0x20
  jz .skip
  cmp rax, 0x9
  jz .skip
  cmp rax, 0xA
  jz .skip
  cmp rax, 0
  jz .end
  mov byte [rdi + r8], al
  inc r8
  .loop:
  call read_char
  cmp rsi, r8
  jng .error
  cmp rax, 0x20
  jz .end
  cmp rax, 0x9
  jz .end
  cmp rax, 0xA
  jz .end
  cmp rax, 0
  jz .end
  mov byte [rdi + r8], al
  inc r8
  jmp .loop
  .end:
  mov byte [rdi + r8], 0
  mov rdx, r8
  mov rax, rdi
  ret
  .error:
  mov rax, 0
  ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
  xor rax, rax ; число
  xor rdx, rdx
  xor r9, r9 ; длина
  mov r10, 10
  .loop:
  xor r8, r8
  mov r8b, [rdi+r9] ;берем цифру числа

  cmp r8b, 0
  je .correct
  cmp r8b, '0'
  jb .incorrect
  cmp r8b, '9'
  ja .incorrect
  sub r8b, '0'

  mul r10
  add rax, r8
  inc r9
  jmp .loop

  .correct:
  mov rdx, r9
  ret
  .incorrect:
  mov rdx, r9
  ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
  xor rax, rax ; число
  xor rdx, rdx ; длина
  cmp byte[rdi], '-' ;берем первую цифру числа
  je .neg
  jmp parse_uint
  .neg:
  inc rdi
  call parse_uint
  cmp rdx, 0
  je .incorrect
  neg rax
  inc rdx
  .incorrect:
  ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
  xor rax, rax
  .loop:
  cmp rax, rdx
  je .failed
  mov r8b, [rdi+rax]
  mov byte [rsi+rax], r8b
  cmp r8, 0
  je .end
  inc rax
  jmp .loop
  .end:
  ret
  .failed:
  mov rax, 0
  ret
